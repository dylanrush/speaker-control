/**
 * This lambda function makes calls to your ESP8266-based remote.
 * The intents here are programmed for the Audioengine A5+ speakers.
 * With your own NEC codes, you should be able to program your own 
 * skill to control any NEC-based infrared device.
 * This lambda is very heavily based on the Alexa example skill.
 * Don't forget to replace YOUR_HOST and YOUR_PORT
 * 
 * By Dylan Rush. blog.dylanhrush.com
 */
// Route the incoming request based on type (LaunchRequest, IntentRequest,
// etc.) The JSON body of the request is provided in the event parameter.
exports.handler = function(event, context) {
    try {
        console.log("event.session.application.applicationId=" + event.session.application.applicationId);

        /**
         * Uncomment this if statement and populate with your skill's application ID to
         * prevent someone else from configuring a skill that sends requests to this function.
         * Note that for this project, your ESP8266 is not really protected anyway...
         */
        /*
        if (event.session.application.applicationId !== "amzn1.echo-sdk-ams.app.[unique-value-here]") {
             context.fail("Invalid Application ID");
         }
        */

        if (event.session.new) {
            onSessionStarted({
                requestId: event.request.requestId
            }, event.session);
        }

        if (event.request.type === "LaunchRequest") {
            onLaunch(event.request,
                event.session,
                function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } else if (event.request.type === "IntentRequest") {
            onIntent(event.request,
                event.session,
                function callback(sessionAttributes, speechletResponse) {
                    context.succeed(buildResponse(sessionAttributes, speechletResponse));
                });
        } else if (event.request.type === "SessionEndedRequest") {
            onSessionEnded(event.request, event.session);
            context.succeed();
        }
    } catch (e) {
        context.fail("Exception: " + e);
    }
};

/**
 * Called when the session starts.
 */
function onSessionStarted(sessionStartedRequest, session) {
    console.log("onSessionStarted requestId=" + sessionStartedRequest.requestId + ", sessionId=" + session.sessionId);
}

/**
 * Called when the user launches the skill without specifying what they want.
 */
function onLaunch(launchRequest, session, callback) {
    console.log("onLaunch requestId=" + launchRequest.requestId + ", sessionId=" + session.sessionId);

    // Dispatch to your skill's launch.
    getWelcomeResponse(callback);
}

/**
 * Called when the user specifies an intent for this skill.
 */
function onIntent(intentRequest, session, callback) {
    console.log("onIntent requestId=" + intentRequest.requestId + ", sessionId=" + session.sessionId);

    var intent = intentRequest.intent,
        intentName = intentRequest.intent.name;

    // Dispatch to your skill's intent handlers
    if ("MuteSpeakers" === intentName) {
        muteSpeakers(callback);
    } else if ("MuteCommercial" === intentName) {
        muteCommercial(callback);
    } else if ("VolumeUp" === intentName) {
        volumeUp(callback);
    } else if ("VolumeDown" === intentName) {
        volumeDown(callback);
    } else {
        throw "Invalid intent: " + intentName;
    }
}

/**
 * Called when the user ends the session.
 * Is not called when the skill returns shouldEndSession=true.
 */
function onSessionEnded(sessionEndedRequest, session) {
    console.log("onSessionEnded requestId=" + sessionEndedRequest.requestId + ", sessionId=" + session.sessionId);
    // Add cleanup logic here
}

// --------------- Functions that control the skill's behavior -----------------------

function muteSpeakers(callback) {
    var callbackForIntent = buildCallbackForHttp(callback, "Muted", "Your speakers have been muted.");
    sendCode(12566591, callbackForIntent);
}

function muteCommercial(callback) {
    // This one might be a little confusing as there are a lot of callbacks.
    // First the initial mute request is made. That calls a callback,
    // onFirstMute, that sets a timeout for the second mute to occur after
    // thirty seconds.
    var callbackFunction = buildCallbackForHttp(callback, "CommercialMuted", "Commercial skipped.");

    var onFirstMute = function() {
        setTimeout(function() {
            sendCode(12566591, callbackFunction);
        }, 30 * 1000);
    }

    sendCode(12566591, onFirstMute);
}

function volumeUp(callback) {
    var callbackFunction = buildCallbackForHttp(callback, "VolumeUp", "Volume up");
    sendCode(12554351, callbackFunction);
}

function volumeDown(callback) {
    var callbackFunction = buildCallbackForHttp(callback, "VolumeDown", "Volume down");
    sendCode(12574751, callbackFunction);
}

function sendCode(code, httpCallback) {
    // Replace YOUR_HOST and YOUR_PORT below.

    var http = require('http');
    var options = {
        host: 'YOUR_HOST',
        port: YOUR_PORT,
        path: "/ir?code=" + code,
        agent: false
    };

    var req = http.get(options, function(res) {
        console.log("Got response: " + res.statusCode);
        console.log("headers: " + res.headers);
        httpCallback(res.statusCode);
    }).on('error', function(e) {
        console.log("Got error message is: " + e.message);
    });

}

function buildCallbackForHttp(callbackForIntent, cardTitle, speechOutput) {
    console.log("buildCallbackForHttp");
    return function(httpResponse) {
        console.log("buildCallbackForHttp was invoked");
        var sessionAttributes = {};
        var shouldEndSession = true;
        if (httpResponse != 200) {
            speechOutput = "I could not communicate with the remote.";
        }
        callbackForIntent(sessionAttributes,
            buildSpeechletResponse(cardTitle, speechOutput, speechOutput, shouldEndSession));
    };
}

function getWelcomeResponse(callback) {
    // If we wanted to initialize the session to have some attributes we could add those here.
    var sessionAttributes = {};
    var cardTitle = "Welcome";
    var speechOutput = "Welcome to your Echo-connected speakers.";
    // If the user either does not reply to the welcome message or says something that is not
    // understood, they will be prompted again with this text.
    var repromptText = "You can use this skill to control the volume or mute your speakers.";
    var shouldEndSession = false;

    callback(sessionAttributes,
        buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession));
}


// --------------- Helpers that build all of the responses -----------------------

function buildSpeechletResponse(title, output, repromptText, shouldEndSession) {
    return {
        outputSpeech: {
            type: "PlainText",
            text: output
        },
        card: {
            type: "Simple",
            title: title,
            content: output
        },
        reprompt: {
            outputSpeech: {
                type: "PlainText",
                text: repromptText
            }
        },
        shouldEndSession: shouldEndSession
    }
}

function buildResponse(sessionAttributes, speechletResponse) {
    return {
        version: "1.0",
        sessionAttributes: sessionAttributes,
        response: speechletResponse
    }
}
