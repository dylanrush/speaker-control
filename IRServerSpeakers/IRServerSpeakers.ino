/*
 * IRremoteESP8266: IRServer - demonstrates sending IR codes controlled from a webserver
 * An IR LED must be connected to ESP8266 pin 0.
 * 
 * For the Alexa-controlled speakers project found at blog.dylanhrush.com
 * 
 * This is heavily based on the IRremoteESP8266.h library and example by Mark Szabo
 * https://github.com/markszabo/IRremoteESP8266
 */

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <IRremoteESP8266.h>
 
const char* ssid = "YOUR-SSID";
const char* password = "YOUR-PASSWORD";

ESP8266WebServer server(80);

IRsend irsend(3);

void handleRoot() {
 // This landing page shows codes for the Audioengine A5+, but you can replace this with anything you want.
 server.send(200, "text/html", "<html><head> <title>ESP8266 Remote Control</title></head><body><h1>Control your Audioengine A5+</h1><p>Usage: http://device/ir?code=1234</p><p>Power = 0xBF807F = 12550271</p><p>Mute = 0xBFC03F = 12566591</p><p>Vol- = 0xBFE01F = 12574751</p><p>Vol+ = 0xBF906F = 12554351</p></body></html>");
}

void successResponse(unsigned long code) {
  char temp[256];
  snprintf(temp, 256, "{\"sentCode\": %ul}", code);
  server.send(200, "application/json", temp);
}

void handleNotFound(){
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void handleIr(){
  for (uint8_t i=0; i<server.args(); i++){
    if(server.argName(i) == "code") 
    {
      unsigned long code = server.arg(i).toInt();
      irsend.sendNEC(code, 36);
      successResponse(code);
      return;
    }
  }
  handleNotFound();
}
 
void setup(void){
  irsend.begin();

  WiFi.begin(ssid, password);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  // Router's static routing puts us at http://192.168.10.196
  //if (mdns.begin("esp8266", WiFi.localIP())) {
  //  Serial.println("MDNS responder started");
  //}
  
  server.on("/", handleRoot);
  server.on("/ir", handleIr); 
 
  server.on("/inline", [](){
    server.send(200, "text/plain", "this works as well");
  });

  server.onNotFound(handleNotFound);
  
  server.begin();
}
 
void loop(void){
  server.handleClient();
  //delay(10);
} 
